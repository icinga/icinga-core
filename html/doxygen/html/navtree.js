var NAVTREE =
[
  [ "Icinga-core", "index.html", [
    [ "Data Structures", "annotated.html", [
      [ "alert_producer_struct", "structalert__producer__struct.html", null ],
      [ "archived_event_struct", "structarchived__event__struct.html", null ],
      [ "archived_state_struct", "structarchived__state__struct.html", null ],
      [ "authdata_struct", "structauthdata__struct.html", null ],
      [ "avail_subject_struct", "structavail__subject__struct.html", null ],
      [ "check_result_struct", "structcheck__result__struct.html", null ],
      [ "check_stats_struct", "structcheck__stats__struct.html", null ],
      [ "circular_buffer_struct", "structcircular__buffer__struct.html", null ],
      [ "command_struct", "structcommand__struct.html", null ],
      [ "commandsmember_struct", "structcommandsmember__struct.html", null ],
      [ "comment_struct", "structcomment__struct.html", null ],
      [ "contact_struct", "structcontact__struct.html", null ],
      [ "contactgroup_struct", "structcontactgroup__struct.html", null ],
      [ "contactgroupsmember_struct", "structcontactgroupsmember__struct.html", null ],
      [ "contactsmember_struct", "structcontactsmember__struct.html", null ],
      [ "customvariablesmember_struct", "structcustomvariablesmember__struct.html", null ],
      [ "daterange_struct", "structdaterange__struct.html", null ],
      [ "dbuf_struct", "structdbuf__struct.html", null ],
      [ "errorlist", "structerrorlist.html", null ],
      [ "escalation_condition_struct", "structescalation__condition__struct.html", null ],
      [ "host_cursor_struct", "structhost__cursor__struct.html", null ],
      [ "host_struct", "structhost__struct.html", null ],
      [ "hostdependency_struct", "structhostdependency__struct.html", null ],
      [ "hostescalation_struct", "structhostescalation__struct.html", null ],
      [ "hostgroup_struct", "structhostgroup__struct.html", null ],
      [ "hostlist", "structhostlist.html", null ],
      [ "hostoutage_struct", "structhostoutage__struct.html", null ],
      [ "hostoutagesort_struct", "structhostoutagesort__struct.html", null ],
      [ "hostsmember_struct", "structhostsmember__struct.html", null ],
      [ "icinga_macros", "structicinga__macros.html", null ],
      [ "ido2db_dbconfig_struct", "structido2db__dbconfig__struct.html", null ],
      [ "ido2db_dbconninfo_struct", "structido2db__dbconninfo__struct.html", null ],
      [ "ido2db_dbobject_struct", "structido2db__dbobject__struct.html", null ],
      [ "ido2db_input_data_info_struct", "structido2db__input__data__info__struct.html", null ],
      [ "ido2db_mbuf_struct", "structido2db__mbuf__struct.html", null ],
      [ "ido_dbuf_struct", "structido__dbuf__struct.html", null ],
      [ "ido_mmapfile_struct", "structido__mmapfile__struct.html", null ],
      [ "idomod_sink_buffer_struct", "structidomod__sink__buffer__struct.html", null ],
      [ "layer_struct", "structlayer__struct.html", null ],
      [ "logentry_filter", "structlogentry__filter.html", null ],
      [ "logentry_struct", "structlogentry__struct.html", null ],
      [ "mmapfile_struct", "structmmapfile__struct.html", null ],
      [ "module_struct", "structmodule__struct.html", null ],
      [ "nagios_extcmd", "structnagios__extcmd.html", null ],
      [ "nagios_macros", "structnagios__macros.html", null ],
      [ "nebcallback_struct", "structnebcallback__struct.html", null ],
      [ "nebmodule_struct", "structnebmodule__struct.html", null ],
      [ "nebstruct_acknowledgement_struct", "structnebstruct__acknowledgement__struct.html", null ],
      [ "nebstruct_adaptive_contact_data_struct", "structnebstruct__adaptive__contact__data__struct.html", null ],
      [ "nebstruct_adaptive_host_data_struct", "structnebstruct__adaptive__host__data__struct.html", null ],
      [ "nebstruct_adaptive_program_data_struct", "structnebstruct__adaptive__program__data__struct.html", null ],
      [ "nebstruct_adaptive_service_data_struct", "structnebstruct__adaptive__service__data__struct.html", null ],
      [ "nebstruct_aggregated_status_struct", "structnebstruct__aggregated__status__struct.html", null ],
      [ "nebstruct_comment_struct", "structnebstruct__comment__struct.html", null ],
      [ "nebstruct_contact_notification_method_struct", "structnebstruct__contact__notification__method__struct.html", null ],
      [ "nebstruct_contact_notification_struct", "structnebstruct__contact__notification__struct.html", null ],
      [ "nebstruct_contact_status_struct", "structnebstruct__contact__status__struct.html", null ],
      [ "nebstruct_downtime_struct", "structnebstruct__downtime__struct.html", null ],
      [ "nebstruct_event_handler_struct", "structnebstruct__event__handler__struct.html", null ],
      [ "nebstruct_external_command_struct", "structnebstruct__external__command__struct.html", null ],
      [ "nebstruct_flapping_struct", "structnebstruct__flapping__struct.html", null ],
      [ "nebstruct_host_check_struct", "structnebstruct__host__check__struct.html", null ],
      [ "nebstruct_host_status_struct", "structnebstruct__host__status__struct.html", null ],
      [ "nebstruct_log_struct", "structnebstruct__log__struct.html", null ],
      [ "nebstruct_notification_struct", "structnebstruct__notification__struct.html", null ],
      [ "nebstruct_process_struct", "structnebstruct__process__struct.html", null ],
      [ "nebstruct_program_status_struct", "structnebstruct__program__status__struct.html", null ],
      [ "nebstruct_retention_struct", "structnebstruct__retention__struct.html", null ],
      [ "nebstruct_service_check_struct", "structnebstruct__service__check__struct.html", null ],
      [ "nebstruct_service_status_struct", "structnebstruct__service__status__struct.html", null ],
      [ "nebstruct_statechange_struct", "structnebstruct__statechange__struct.html", null ],
      [ "nebstruct_system_command_struct", "structnebstruct__system__command__struct.html", null ],
      [ "nebstruct_timed_event_struct", "structnebstruct__timed__event__struct.html", null ],
      [ "notify_list_struct", "structnotify__list__struct.html", null ],
      [ "objectlist_struct", "structobjectlist__struct.html", null ],
      [ "passive_check_result_struct", "structpassive__check__result__struct.html", null ],
      [ "pr_chunk", "structpr__chunk.html", null ],
      [ "pr_chunk_x", "structpr__chunk__x.html", null ],
      [ "profile_object", "structprofile__object.html", null ],
      [ "profiler_item", "structprofiler__item.html", null ],
      [ "sched_info_struct", "structsched__info__struct.html", null ],
      [ "scheduled_downtime_struct", "structscheduled__downtime__struct.html", null ],
      [ "service_struct", "structservice__struct.html", null ],
      [ "servicedependency_struct", "structservicedependency__struct.html", null ],
      [ "serviceescalation_struct", "structserviceescalation__struct.html", null ],
      [ "servicegroup_struct", "structservicegroup__struct.html", null ],
      [ "servicesmember_struct", "structservicesmember__struct.html", null ],
      [ "skiplist_struct", "structskiplist__struct.html", null ],
      [ "skiplistnode_struct", "structskiplistnode__struct.html", null ],
      [ "sort_struct", "structsort__struct.html", null ],
      [ "sortdata_struct", "structsortdata__struct.html", null ],
      [ "statusdata_struct", "structstatusdata__struct.html", null ],
      [ "timed_event_struct", "structtimed__event__struct.html", null ],
      [ "timeperiod_struct", "structtimeperiod__struct.html", null ],
      [ "timeperiodexclusion_struct", "structtimeperiodexclusion__struct.html", null ],
      [ "timerange_struct", "structtimerange__struct.html", null ],
      [ "timeslice_data_struct", "structtimeslice__data__struct.html", null ],
      [ "xodtemplate_command_struct", "structxodtemplate__command__struct.html", null ],
      [ "xodtemplate_contact_struct", "structxodtemplate__contact__struct.html", null ],
      [ "xodtemplate_contactgroup_struct", "structxodtemplate__contactgroup__struct.html", null ],
      [ "xodtemplate_contactlist_struct", "structxodtemplate__contactlist__struct.html", null ],
      [ "xodtemplate_customvariablesmember_struct", "structxodtemplate__customvariablesmember__struct.html", null ],
      [ "xodtemplate_daterange_struct", "structxodtemplate__daterange__struct.html", null ],
      [ "xodtemplate_escalation_condition_struct", "structxodtemplate__escalation__condition__struct.html", null ],
      [ "xodtemplate_host_struct", "structxodtemplate__host__struct.html", null ],
      [ "xodtemplate_hostdependency_struct", "structxodtemplate__hostdependency__struct.html", null ],
      [ "xodtemplate_hostescalation_struct", "structxodtemplate__hostescalation__struct.html", null ],
      [ "xodtemplate_hostextinfo_struct", "structxodtemplate__hostextinfo__struct.html", null ],
      [ "xodtemplate_hostgroup_struct", "structxodtemplate__hostgroup__struct.html", null ],
      [ "xodtemplate_hostlist_struct", "structxodtemplate__hostlist__struct.html", null ],
      [ "xodtemplate_memberlist_struct", "structxodtemplate__memberlist__struct.html", null ],
      [ "xodtemplate_module_struct", "structxodtemplate__module__struct.html", null ],
      [ "xodtemplate_service_cursor_struct", "structxodtemplate__service__cursor__struct.html", null ],
      [ "xodtemplate_service_struct", "structxodtemplate__service__struct.html", null ],
      [ "xodtemplate_servicedependency_struct", "structxodtemplate__servicedependency__struct.html", null ],
      [ "xodtemplate_serviceescalation_struct", "structxodtemplate__serviceescalation__struct.html", null ],
      [ "xodtemplate_serviceextinfo_struct", "structxodtemplate__serviceextinfo__struct.html", null ],
      [ "xodtemplate_servicegroup_struct", "structxodtemplate__servicegroup__struct.html", null ],
      [ "xodtemplate_servicelist_struct", "structxodtemplate__servicelist__struct.html", null ],
      [ "xodtemplate_timeperiod_struct", "structxodtemplate__timeperiod__struct.html", null ]
    ] ],
    [ "Data Structure Index", "classes.html", null ],
    [ "Data Fields", "functions.html", null ],
    [ "File List", "files.html", [
      [ "base/broker.c", "broker_8c.html", null ],
      [ "base/checks.c", "checks_8c.html", null ],
      [ "base/commands.c", "commands_8c.html", null ],
      [ "base/config.c", "base_2config_8c.html", null ],
      [ "base/events.c", "events_8c.html", null ],
      [ "base/flapping.c", "flapping_8c.html", null ],
      [ "base/icinga.c", "icinga_8c.html", null ],
      [ "base/icingastats.c", "icingastats_8c.html", null ],
      [ "base/logging.c", "logging_8c.html", null ],
      [ "base/nebmods.c", "nebmods_8c.html", null ],
      [ "base/netutils.c", "netutils_8c.html", null ],
      [ "base/notifications.c", "base_2notifications_8c.html", null ],
      [ "base/perfdata.c", "perfdata_8c.html", null ],
      [ "base/profiler.c", "profiler_8c.html", null ],
      [ "base/sehandlers.c", "sehandlers_8c.html", null ],
      [ "base/sretention.c", "sretention_8c.html", null ],
      [ "base/statsprofiler.c", "statsprofiler_8c.html", null ],
      [ "base/utils.c", "base_2utils_8c.html", null ],
      [ "cgi/avail.c", "avail_8c.html", null ],
      [ "cgi/cgiauth.c", "cgiauth_8c.html", null ],
      [ "cgi/cgiutils.c", "cgiutils_8c.html", null ],
      [ "cgi/cmd.c", "cmd_8c.html", null ],
      [ "cgi/config.c", "cgi_2config_8c.html", null ],
      [ "cgi/extcmd_list.c", "extcmd__list_8c.html", null ],
      [ "cgi/extinfo.c", "extinfo_8c.html", null ],
      [ "cgi/getcgi.c", "getcgi_8c.html", null ],
      [ "cgi/histogram.c", "histogram_8c.html", null ],
      [ "cgi/history.c", "history_8c.html", null ],
      [ "cgi/notifications.c", "cgi_2notifications_8c.html", null ],
      [ "cgi/outages.c", "outages_8c.html", null ],
      [ "cgi/readlogs.c", "readlogs_8c.html", null ],
      [ "cgi/showlog.c", "showlog_8c.html", null ],
      [ "cgi/status.c", "status_8c.html", null ],
      [ "cgi/statusmap.c", "statusmap_8c.html", null ],
      [ "cgi/statuswml.c", "statuswml_8c.html", null ],
      [ "cgi/statuswrl.c", "statuswrl_8c.html", null ],
      [ "cgi/summary.c", "summary_8c.html", null ],
      [ "cgi/tac.c", "tac_8c.html", null ],
      [ "cgi/trends.c", "trends_8c.html", null ],
      [ "common/comments.c", "comments_8c.html", null ],
      [ "common/downtime.c", "downtime_8c.html", null ],
      [ "common/macros.c", "macros_8c.html", null ],
      [ "common/objects.c", "objects_8c.html", null ],
      [ "common/shared.c", "shared_8c.html", null ],
      [ "common/skiplist.c", "skiplist_8c.html", null ],
      [ "common/snprintf.c", "snprintf_8c.html", null ],
      [ "common/statusdata.c", "statusdata_8c.html", null ],
      [ "contrib/convertcfg.c", "convertcfg_8c.html", null ],
      [ "contrib/daemonchk.c", "daemonchk_8c.html", null ],
      [ "contrib/epn_icinga.h", "contrib_2epn__icinga_8h.html", null ],
      [ "contrib/mini_epn.c", "mini__epn_8c.html", null ],
      [ "contrib/new_mini_epn.c", "new__mini__epn_8c.html", null ],
      [ "include/broker.h", "broker_8h.html", null ],
      [ "include/cgiauth.h", "cgiauth_8h.html", null ],
      [ "include/cgiutils.h", "cgiutils_8h.html", null ],
      [ "include/comments.h", "comments_8h.html", null ],
      [ "include/common.h", "include_2common_8h.html", null ],
      [ "include/config.h", "config_8h.html", null ],
      [ "include/downtime.h", "downtime_8h.html", null ],
      [ "include/epn_icinga.h", "include_2epn__icinga_8h.html", null ],
      [ "include/getcgi.h", "getcgi_8h.html", null ],
      [ "include/icinga.h", "icinga_8h.html", null ],
      [ "include/locations.h", "locations_8h.html", null ],
      [ "include/logging.h", "logging_8h.html", null ],
      [ "include/macros.h", "macros_8h.html", null ],
      [ "include/nebcallbacks.h", "nebcallbacks_8h.html", null ],
      [ "include/neberrors.h", "neberrors_8h.html", null ],
      [ "include/nebmods.h", "nebmods_8h.html", null ],
      [ "include/nebmodules.h", "nebmodules_8h.html", null ],
      [ "include/nebstructs.h", "nebstructs_8h.html", null ],
      [ "include/netutils.h", "netutils_8h.html", null ],
      [ "include/objects.h", "objects_8h.html", null ],
      [ "include/perfdata.h", "perfdata_8h.html", null ],
      [ "include/profiler.h", "profiler_8h.html", null ],
      [ "include/readlogs.h", "readlogs_8h.html", null ],
      [ "include/shared.h", "shared_8h.html", null ],
      [ "include/skiplist.h", "skiplist_8h.html", null ],
      [ "include/snprintf.h", "snprintf_8h.html", null ],
      [ "include/sretention.h", "sretention_8h.html", null ],
      [ "include/statsprofiler.h", "statsprofiler_8h.html", null ],
      [ "include/statusdata.h", "statusdata_8h.html", null ],
      [ "module/helloworld.c", "helloworld_8c.html", null ],
      [ "module/idoutils/include/common.h", "module_2idoutils_2include_2common_8h.html", null ],
      [ "module/idoutils/include/db.h", "db_8h.html", null ],
      [ "module/idoutils/include/dbhandlers.h", "dbhandlers_8h.html", null ],
      [ "module/idoutils/include/dbqueries.h", "dbqueries_8h.html", null ],
      [ "module/idoutils/include/ido2db.h", "ido2db_8h.html", null ],
      [ "module/idoutils/include/idomod.h", "idomod_8h.html", null ],
      [ "module/idoutils/include/io.h", "io_8h.html", null ],
      [ "module/idoutils/include/protoapi.h", "protoapi_8h.html", null ],
      [ "module/idoutils/include/utils.h", "utils_8h.html", null ],
      [ "module/idoutils/src/db.c", "db_8c.html", null ],
      [ "module/idoutils/src/dbhandlers.c", "dbhandlers_8c.html", null ],
      [ "module/idoutils/src/dbqueries.c", "dbqueries_8c.html", null ],
      [ "module/idoutils/src/file2sock.c", "file2sock_8c.html", null ],
      [ "module/idoutils/src/ido2db.c", "ido2db_8c.html", null ],
      [ "module/idoutils/src/idomod.c", "idomod_8c.html", null ],
      [ "module/idoutils/src/io.c", "io_8c.html", null ],
      [ "module/idoutils/src/log2ido.c", "log2ido_8c.html", null ],
      [ "module/idoutils/src/protonum.c", "protonum_8c.html", null ],
      [ "module/idoutils/src/sockdebug.c", "sockdebug_8c.html", null ],
      [ "module/idoutils/src/utils.c", "module_2idoutils_2src_2utils_8c.html", null ],
      [ "xdata/xcddefault.c", "xcddefault_8c.html", null ],
      [ "xdata/xcddefault.h", "xcddefault_8h.html", null ],
      [ "xdata/xdddefault.c", "xdddefault_8c.html", null ],
      [ "xdata/xdddefault.h", "xdddefault_8h.html", null ],
      [ "xdata/xodtemplate.c", "xodtemplate_8c.html", null ],
      [ "xdata/xodtemplate.h", "xodtemplate_8h.html", null ],
      [ "xdata/xpddefault.c", "xpddefault_8c.html", null ],
      [ "xdata/xpddefault.h", "xpddefault_8h.html", null ],
      [ "xdata/xrddefault.c", "xrddefault_8c.html", null ],
      [ "xdata/xrddefault.h", "xrddefault_8h.html", null ],
      [ "xdata/xsddefault.c", "xsddefault_8c.html", null ],
      [ "xdata/xsddefault.h", "xsddefault_8h.html", null ]
    ] ],
    [ "Globals", "globals.html", null ]
  ] ]
];

function createIndent(o,domNode,node,level)
{
  if (node.parentNode && node.parentNode.parentNode)
  {
    createIndent(o,domNode,node.parentNode,level+1);
  }
  var imgNode = document.createElement("img");
  if (level==0 && node.childrenData)
  {
    node.plus_img = imgNode;
    node.expandToggle = document.createElement("a");
    node.expandToggle.href = "javascript:void(0)";
    node.expandToggle.onclick = function() 
    {
      if (node.expanded) 
      {
        $(node.getChildrenUL()).slideUp("fast");
        if (node.isLast)
        {
          node.plus_img.src = node.relpath+"ftv2plastnode.png";
        }
        else
        {
          node.plus_img.src = node.relpath+"ftv2pnode.png";
        }
        node.expanded = false;
      } 
      else 
      {
        expandNode(o, node, false);
      }
    }
    node.expandToggle.appendChild(imgNode);
    domNode.appendChild(node.expandToggle);
  }
  else
  {
    domNode.appendChild(imgNode);
  }
  if (level==0)
  {
    if (node.isLast)
    {
      if (node.childrenData)
      {
        imgNode.src = node.relpath+"ftv2plastnode.png";
      }
      else
      {
        imgNode.src = node.relpath+"ftv2lastnode.png";
        domNode.appendChild(imgNode);
      }
    }
    else
    {
      if (node.childrenData)
      {
        imgNode.src = node.relpath+"ftv2pnode.png";
      }
      else
      {
        imgNode.src = node.relpath+"ftv2node.png";
        domNode.appendChild(imgNode);
      }
    }
  }
  else
  {
    if (node.isLast)
    {
      imgNode.src = node.relpath+"ftv2blank.png";
    }
    else
    {
      imgNode.src = node.relpath+"ftv2vertline.png";
    }
  }
  imgNode.border = "0";
}

function newNode(o, po, text, link, childrenData, lastNode)
{
  var node = new Object();
  node.children = Array();
  node.childrenData = childrenData;
  node.depth = po.depth + 1;
  node.relpath = po.relpath;
  node.isLast = lastNode;

  node.li = document.createElement("li");
  po.getChildrenUL().appendChild(node.li);
  node.parentNode = po;

  node.itemDiv = document.createElement("div");
  node.itemDiv.className = "item";

  node.labelSpan = document.createElement("span");
  node.labelSpan.className = "label";

  createIndent(o,node.itemDiv,node,0);
  node.itemDiv.appendChild(node.labelSpan);
  node.li.appendChild(node.itemDiv);

  var a = document.createElement("a");
  node.labelSpan.appendChild(a);
  node.label = document.createTextNode(text);
  a.appendChild(node.label);
  if (link) 
  {
    a.href = node.relpath+link;
  } 
  else 
  {
    if (childrenData != null) 
    {
      a.className = "nolink";
      a.href = "javascript:void(0)";
      a.onclick = node.expandToggle.onclick;
      node.expanded = false;
    }
  }

  node.childrenUL = null;
  node.getChildrenUL = function() 
  {
    if (!node.childrenUL) 
    {
      node.childrenUL = document.createElement("ul");
      node.childrenUL.className = "children_ul";
      node.childrenUL.style.display = "none";
      node.li.appendChild(node.childrenUL);
    }
    return node.childrenUL;
  };

  return node;
}

function showRoot()
{
  var headerHeight = $("#top").height();
  var footerHeight = $("#nav-path").height();
  var windowHeight = $(window).height() - headerHeight - footerHeight;
  navtree.scrollTo('#selected',0,{offset:-windowHeight/2});
}

function expandNode(o, node, imm)
{
  if (node.childrenData && !node.expanded) 
  {
    if (!node.childrenVisited) 
    {
      getNode(o, node);
    }
    if (imm)
    {
      $(node.getChildrenUL()).show();
    } 
    else 
    {
      $(node.getChildrenUL()).slideDown("fast",showRoot);
    }
    if (node.isLast)
    {
      node.plus_img.src = node.relpath+"ftv2mlastnode.png";
    }
    else
    {
      node.plus_img.src = node.relpath+"ftv2mnode.png";
    }
    node.expanded = true;
  }
}

function getNode(o, po)
{
  po.childrenVisited = true;
  var l = po.childrenData.length-1;
  for (var i in po.childrenData) 
  {
    var nodeData = po.childrenData[i];
    po.children[i] = newNode(o, po, nodeData[0], nodeData[1], nodeData[2],
        i==l);
  }
}

function findNavTreePage(url, data)
{
  var nodes = data;
  var result = null;
  for (var i in nodes) 
  {
    var d = nodes[i];
    if (d[1] == url) 
    {
      return new Array(i);
    }
    else if (d[2] != null) // array of children
    {
      result = findNavTreePage(url, d[2]);
      if (result != null) 
      {
        return (new Array(i).concat(result));
      }
    }
  }
  return null;
}

function initNavTree(toroot,relpath)
{
  var o = new Object();
  o.toroot = toroot;
  o.node = new Object();
  o.node.li = document.getElementById("nav-tree-contents");
  o.node.childrenData = NAVTREE;
  o.node.children = new Array();
  o.node.childrenUL = document.createElement("ul");
  o.node.getChildrenUL = function() { return o.node.childrenUL; };
  o.node.li.appendChild(o.node.childrenUL);
  o.node.depth = 0;
  o.node.relpath = relpath;

  getNode(o, o.node);

  o.breadcrumbs = findNavTreePage(toroot, NAVTREE);
  if (o.breadcrumbs == null)
  {
    o.breadcrumbs = findNavTreePage("index.html",NAVTREE);
  }
  if (o.breadcrumbs != null && o.breadcrumbs.length>0)
  {
    var p = o.node;
    for (var i in o.breadcrumbs) 
    {
      var j = o.breadcrumbs[i];
      p = p.children[j];
      expandNode(o,p,true);
    }
    p.itemDiv.className = p.itemDiv.className + " selected";
    p.itemDiv.id = "selected";
    $(window).load(showRoot);
  }
}

