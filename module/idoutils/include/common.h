/************************************************************************
 *
 * COMMON.H - Protocol Definition
 * Copyright (c) 2005 Ethan Galstad
 * Copyright (c) 2009-2011 Icinga Development Team (http://www.icinga.org)
 *
 ************************************************************************/

#ifndef _IDO_COMMON_H
#define _IDO_COMMON_H



#define IDO_TRUE      1
#define IDO_FALSE     0

#define IDO_ERROR     -1
#define IDO_OK        0


#endif
